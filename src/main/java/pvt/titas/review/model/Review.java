package pvt.titas.review.model;

import lombok.*;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Review {
    private HostInfo hostInfo;
    private Integer id;
    private String title;
    private String author;
    private String reviewText;
    private Double rating;
}
