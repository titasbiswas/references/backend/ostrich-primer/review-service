package pvt.titas.review.model;

import lombok.*;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class HostInfo {
    private Integer port;
    private String host, ip;
}
