package pvt.titas.review.HealthCheck;

import com.codahale.metrics.health.HealthCheck;
import pvt.titas.review.ReviewService;

import javax.ws.rs.core.Response;

public class ReviewServiceHealthCheck extends HealthCheck {
    @Override
    protected Result check() throws Exception {
        if (ReviewService.STATUS_OVERRIDE == Response.Status.OK) {
            return Result.healthy();
        } else {
            return Result.unhealthy("unhealthy by toggle");
        }
    }
}
