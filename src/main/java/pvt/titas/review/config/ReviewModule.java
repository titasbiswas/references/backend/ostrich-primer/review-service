package pvt.titas.review.config;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import pvt.titas.review.data.ReviewData;
import pvt.titas.review.data.ReviewDataImpl;
import pvt.titas.review.model.HostInfo;

public class ReviewModule extends AbstractModule {

    @Override
    protected void configure() {
        //super.configure();
        bind(ReviewData.class).to(ReviewDataImpl.class).asEagerSingleton();
        //bind(HostInfo.class).to(HostInfo.class).asEagerSingleton();
    }

    @Provides
    @Singleton
    private HostInfo hostInfo(){
        return new HostInfo();
    }
}
