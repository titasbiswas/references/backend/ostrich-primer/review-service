package pvt.titas.review.config;

import com.bazaarvoice.curator.dropwizard.ZooKeeperConfiguration;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ReviewServiceConfiguration extends Configuration {
    @NotNull
    @Valid
    @JsonProperty
    private final ZooKeeperConfiguration zooKeeper = new ZooKeeperConfiguration();

    @NotNull
    @NotEmpty
    @JsonProperty
    private  String appName;

    public ZooKeeperConfiguration getZooKeeperConfiguration() {
        return zooKeeper;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }
}
