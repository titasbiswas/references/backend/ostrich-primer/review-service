package pvt.titas.review.data;

import pvt.titas.review.model.Review;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ReviewDataImpl implements ReviewData {

    List<Review> reviewData;

    @Override
    public void initData() {
        reviewData = IntStream.range(1, 10).mapToObj(i -> {
            Review review = Review.builder()
                    .id(i)
                    .author("Author " + i)
                    .rating(i + 1.0)
                    .reviewText("Awesome product " + i)
                    .title("Product " + i)
                    .build();

            return review;
        }).collect(Collectors.toList());
        //.collect(ImmutableList.toImmutableList());
    }

    @Override
    public List<Review> getAll(){
        return reviewData;
    }

    @Override
    public Optional<Review> getById(Integer id){
        return reviewData.stream().filter(d->d.getId()==id).findFirst();
    }

    @Override
    public Review addReview(Review review){
        review.setId(reviewData.size());
        reviewData.add(review);
        return  review;
    }
}
