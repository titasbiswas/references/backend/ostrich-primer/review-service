package pvt.titas.review.data;

import pvt.titas.review.model.Review;

import java.util.List;
import java.util.Optional;

public interface ReviewData {
    void initData();

    List<Review> getAll();

    Optional<Review> getById(Integer id);

    Review addReview(Review review);
}
