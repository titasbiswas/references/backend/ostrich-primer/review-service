package pvt.titas.review;

import com.bazaarvoice.ostrich.ServiceEndPoint;
import com.bazaarvoice.ostrich.ServiceEndPointBuilder;
import com.bazaarvoice.ostrich.registry.zookeeper.ZooKeeperServiceRegistry;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Guice;
import com.google.inject.Injector;
import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.jetty.ConnectorFactory;
import io.dropwizard.jetty.HttpConnectorFactory;
import io.dropwizard.lifecycle.Managed;
import io.dropwizard.server.DefaultServerFactory;
import io.dropwizard.server.ServerFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.apache.curator.framework.CuratorFramework;
import pvt.titas.review.config.ReviewModule;
import pvt.titas.review.config.ReviewServiceConfiguration;
import pvt.titas.review.data.ReviewData;
import pvt.titas.review.model.HostInfo;
import pvt.titas.review.resource.ReviewResource;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.InetAddress;
import java.net.URI;
import java.util.List;
import java.util.Map;

public class ReviewService extends Application<ReviewServiceConfiguration> {

    public static Response.Status STATUS_OVERRIDE = Response.Status.OK;
    private String appName;

    @Override
    public String getName() {
        return appName;
    }

    @Override
    public void initialize(Bootstrap<ReviewServiceConfiguration> bootstrap) {
    }

    public void run(ReviewServiceConfiguration config, Environment env) throws Exception {
        //aquire instace info
        InetAddress localhost = InetAddress.getLocalHost();
        String host = localhost.getHostName();
        String ip = localhost.getHostAddress();
        int port = getHttpPort(config);
        int adminPort = getAdminHttpPort(config);

        //init
        this.appName = config.getAppName();
        Injector injector = Guice.createInjector(new ReviewModule());
        ReviewData reviewData = injector.getInstance(ReviewData.class);
        reviewData.initData();
        HostInfo hostInfo = injector.getInstance(HostInfo.class);
        hostInfo.setPort(port);
        hostInfo.setHost(host);
        hostInfo.setIp(ip);
        ReviewResource reviewResource = injector.getInstance(ReviewResource.class);



        //register resources
        env.jersey().register(reviewResource);



        // The client reads the URLs out of the payload to figure out how to connect to this server.
        URI serviceUri = UriBuilder.fromResource(ReviewResource.class).scheme("http").host(ip).port(port).build();
        URI adminUri = UriBuilder.fromPath("").scheme("http").host(ip).port(adminPort).build();
        Map<String, ?> payload = ImmutableMap.of(
                "url", serviceUri,
                "adminUrl", adminUri);
        final ServiceEndPoint endPoint = new ServiceEndPointBuilder()
                //.withServiceName(env.getName())
                .withServiceName(appName)
                .withId(host + ":" + port)
                .withPayload(env.getObjectMapper().writeValueAsString(payload))
                .build();

        final CuratorFramework curator = config.getZooKeeperConfiguration().newManagedCurator(env.lifecycle());
        env.lifecycle().manage(new Managed() {
            ZooKeeperServiceRegistry registry = new ZooKeeperServiceRegistry(curator, env.metrics());

            @Override
            public void start() throws Exception {
                registry.register(endPoint);
            }

            @Override
            public void stop() throws Exception {
                registry.unregister(endPoint);
            }
        });
    }

    private int getHttpPort(Configuration config) {
        ServerFactory serverFactory = config.getServerFactory();
        if (!(serverFactory instanceof DefaultServerFactory)) {
            throw new IllegalStateException("Server factory is not an instance of DefaultServerFactory");
        }

        List<ConnectorFactory> connectors = ((DefaultServerFactory) serverFactory).getApplicationConnectors();
        for (ConnectorFactory connector : connectors) {
            if (connector instanceof HttpConnectorFactory) {
                return ((HttpConnectorFactory) connector).getPort();
            }
        }

        throw new IllegalStateException("Unable to determine HTTP port");
    }

    private int getAdminHttpPort(Configuration config) {
        ServerFactory serverFactory = config.getServerFactory();
        if (!(serverFactory instanceof DefaultServerFactory)) {
            throw new IllegalStateException("Server factory is not an instance of DefaultServerFactory");
        }

        List<ConnectorFactory> connectors = ((DefaultServerFactory) serverFactory).getAdminConnectors();
        for (ConnectorFactory connector : connectors) {
            if (connector instanceof HttpConnectorFactory) {
                return ((HttpConnectorFactory) connector).getPort();
            }
        }

        throw new IllegalStateException("Unable to determine admin HTTP port");
    }

    public static void main(String[] args) throws Exception {
        new ReviewService().run(args);
    }
}
