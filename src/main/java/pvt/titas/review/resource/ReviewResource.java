package pvt.titas.review.resource;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import pvt.titas.review.data.ReviewData;
import pvt.titas.review.model.HostInfo;
import pvt.titas.review.model.Review;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/reviews/v1")
@Produces(MediaType.APPLICATION_JSON)
public class ReviewResource {

    ReviewResource(){

    }

    @Inject
    private HostInfo hostInfo;

    @Inject
    private ReviewData reviewData;

    @GET
    @Timed
    public List<Review> getAllReview() {
        return reviewData.getAll();
    }

    @GET
    @Timed
    @Path("/{id}")
    public Review getReviewById(@PathParam("id") Integer id) {
        Review review =
                reviewData.getById(id).orElseGet(()->new Review());
        review.setHostInfo(this.hostInfo);
        return review;
    }

    @POST
    @Timed
    public Review createReview(Review review) {
        return reviewData.addReview(review);
    }
}
